/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise3;

import java.util.Scanner;

/**
 *
 * @author HienDepTrai
 */
public class FrequentNumber {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter size: ");
        int n = sc.nextInt();
        int[] ar = new int[n];
        int i = 0;
        String c = "";
        //Input elements of array
        do {
            System.out.print("Enter number " + i + ": ");
            ar[i] = sc.nextInt();
            i++;
            System.out.println("Do you want to continues: Yes:Y/y  No:N/n");
            c = sc.next();
        } while (i < n && c.equalsIgnoreCase("Y"));
        System.out.println("Maximum size of array!");

        System.out.print("Array: ");
        //Display array
        for (i = 0; i < n; i++) {
            System.out.print(ar[i] + "  ");
        }

        System.out.println("\n");
        System.out.println("Enter Value: ");
        int value = sc.nextInt();
        int count = 0;
        //count the amount of frequence
        for (i = 0; i < n; i++) {
            if (ar[i] == value) {
                count++;
            }
        }
        System.out.println("Amount of frequence: " + count);
        if (count != 0) {
            System.out.print("Index: ");
            //prints amount and positions.
            for (i = 0; i < n; i++) {
                if (ar[i] == value) {

                    System.out.print(i + "  ");
                }
            }
        }
        System.out.println("");
    }
}
