/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise4;

import java.util.Scanner;

/**
 *
 * @author HienDepTrai
 */
public class ArrayReverse {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter size: ");
        int n = sc.nextInt();
        int[] ar = new int[n];
        int i = 0;
        //Input elements of array
        do {
            System.out.print("Enter number " + i + ": ");
            ar[i] = sc.nextInt();
            i++;
        } while (i < n);

        System.out.print("Original Array: ");
        //Display array
        for (i = 0; i < n; i++) {
            System.out.print(ar[i] + "  ");
        }

        //reverse the array
        for (int j = 0; j < ar.length / 2; j++) {
            int temp = ar[j];
            ar[j] = ar[n - 1 - j];
            ar[n - 1 - j] = temp;
        }
        //Display array after reversed
        System.out.print("\nReversed Array: ");
        for (int j = 0; j < ar.length; j++) {
            System.out.print(ar[j] + "  ");
        }

    }
}
