/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise2;

import java.util.Scanner;

/**
 *
 * @author HienDepTrai
 */
public class ArrayContains {

    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter length: ");
        int n = sc.nextInt();
        String[] sa = new String[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Input element " + i + " :");
            sa[i] = sc.next();

        }
        for (int i = 0; i < n; i++) {
            System.out.print(sa[i] + "  ");
        }
        System.out.println("\n----------------------------");
        System.out.print("Enter a string variable: ");
        String sValue = sc.next();
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (sValue.toLowerCase().equals(sa[i].toLowerCase())) {
                count++;
            }
        }
        if (count == 0) {
            System.out.println("Check '" + sValue + "' in Array" + ": Not contained!");
        } else {
            System.out.println("Check '" + sValue + "' in Array" + ": Contained!");
        }
    }
}
