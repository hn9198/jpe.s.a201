/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise1;

import java.util.Scanner;

/**
 *
 * @author HienDepTrai
 */
public class SumAverageRunningInt {

    public static void main(String[] args) {
        int n;
        int s = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Input range: ");
        n = sc.nextInt();
        for (int i = 1; i <= n; i++) {
            s += i;
        }
        System.out.println("Average of all " + n + " first numbers: " + Math.floor(s / n));
    }
}
